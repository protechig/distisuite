<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
global $product;
// Ensure visibility.
if ( empty(
 $product
	) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
<div class="product-wraper">
	<div class="product-image hide">
	<a href="<?php the_permalink(); ?>">

		<?php
			/**
			 * Woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
	</a>
	</div>
		<!-- details -->
		<div class="product-content hide">
			<div class="discription">
				<!-- header -->
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<a href="<?php the_field( 'pdf_file' ); ?>"><span><i class="far fa-file-pdf"></i></span></a>
				</h3>
				<!-- end header -->
				<?php
				$product_mfg = wp_get_post_terms( get_the_id(), 'manufacturers' );
				if (count($product_mfg) != 0) {
					$product_mfg = $product_mfg[0];
				}
				
				// Manufacturer exists.
				if ( $product_mfg->term_id ){
				?>
				<a href=<?php echo get_term_link( $product_mfg->term_id ); ?>><?php echo $product_mfg->name; ?></a>
				<?php
				}
				?>
				<div class="dist-cat">
					<?php echo $product->get_categories(); ?>
				</div>	
				<p><?php echo $post->post_excerpt; ?></p>
			</div>
			<div class="price-details">
				<h4><?php echo $product->get_stock_quantity(); ?> available</h4>
				<?php
				/**
				 * Woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
				do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
		</div>
		<!-- end details -->
</div>
	
</li>
