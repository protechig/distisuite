<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */




defined( 'ABSPATH' ) || exit;
global $product;
// Ensure visibility.
if ( empty(
 $product
	) || ! $product->is_visible() ) {
	return;
}

?>
<div class="inner-grid">
			<?php
				$args = array(
					'delimiter' => ' / ',
				);
			woocommerce_breadcrumb( $args );
			?>
			<h3 itemprop="name" class="product-category">
				<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
			</h3>
	<div class="single-product">
		<div class="product-status">
				<div class="product-image">
				  <?php //do_action( 'woocommerce_product_thumbnails' ); ?>
					<img src="<?php echo esc_url( get_the_post_thumbnail_url( $loop->post->ID) ); ?>" class="img-responsive" alt=""/>
				</div>
				<div class="pdf-button">
				<a class="button linecard-btn" href="<?php the_field( 'pdf_file' ); ?>">Download <i class="far fa-file-pdf"></i></a>
				</div>
				<!-- product info -->
				<div class="product-short-description">
					<h2 itemprop="name" class="product_title"><?php the_title(); ?></h2>
					<table class="price-break">
						<tr>
							<td>Descriptions</td>
							<td><?php echo esc_html( $post->post_excerpt ); ?></td>
						</tr>
						<tr>
							<td>Manufacturer</td>
							<td>
							<?php
							$terms = get_the_terms( $post->ID, 'manufacturers' );
							if ( $terms && ! is_wp_error( $terms ) ) {
								echo esc_html( $terms[0]->name );
								}
							?>
							<br><a href="<?php the_field( 'website_url', $terms[0]); ?>"><?php the_field( 'website_url', $terms[0] ); ?></a>
								</td>
							</tr>
							<tr>
								<td>Qty Available</td>
								<td><?php echo esc_html( $product->get_stock_quantity() ); ?></td>
							</tr>
					</table>
				</div>

		</div>

		<div class="product-description">
			<div class="add-tocart">
				<?php
				    	
					if ( ! $product->is_in_stock() ) {
					
					echo esc_url( 'OUT OF STOCK :  <a href="' . home_url( '/quotation?product_number=' . get_the_title() . '&manufacturer=' . $product_mfg->name ) . ' " rel="nofollow" class="button linecard-btn " >Get A Quote</a> ' );
					} else {
					echo '<h3 class="product-price"> PRICE :  ' . $product->get_price_html() . '</h3>';
					do_action( 'woocommerce_simple_add_to_cart' );
					}
				?>
				
				</div>
				<div class="price-break">
				<?php do_action( 'woocommerce_product_meta_end' ); ?>
				</div>
				<div class="quotation-button">
				<div class="need-more">Need volume? <a class="button linecard-btn" href="<?php echo esc_url( site_url( '/request-a-quote?product_number=' . get_the_title() . '&manufacturer=' . $product_mfg->name ) ); ?>">Get A Quote</a></div>	
				</div>
				
		</div>
	</div>
		<div class="modified-staus">
			<table class="part_table" id="other_fields">
				<tbody>
				<tr>
					<td >Category:</td>
					<td>
					<h3 class="product-category"><?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?></h3>
					</td>
					<td>Inventory Refreshed:</td>
					<td ><?php echo get_the_date(); ?></td>
				</tr>
				<tr>
					<td>Rohs Status:</td>
					<td> <?php echo esc_html( the_field( 'rohs_status', $term ) ); ?></td>
					<td>Last modified:</td>
					<td > <?php echo the_modified_date( $d, $before, $after ); ?></td>
				</tr>
				</tbody>
			</table>
		</div>

</div>
