<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
 exit;
}

get_header( 'shop' );
?>


<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="inner-grid">
			<div class="header">
				<h2 class="title">Categories</h2>
			</div>
			<div>
				<?php
				// WP_Term_Query arguments.
				$args = array(
					'taxonomy'     => array( 'product_cat' ),
					'hierarchical' => false,
					'parent'       => 0,
				);

				// The Term Query.
				$term_query = new WP_Term_Query( $args );

				// The Loop.
				if ( ! empty( $term_query ) && ! is_wp_error( $term_query ) ) {
					$product_terms = $term_query->terms;
					foreach ( $product_terms as $term ) {
						?>
						<div class="category-product-wrapper row">
							<div class="product-image">
								<?php
								$thumbnail_id = get_woocommerce_term_meta( $term->term_taxonomy_id, 'thumbnail_id', true );
								$image        = wp_get_attachment_url( $thumbnail_id );
								echo '<img src=' . esc_html( $image ) . ' >';
								?>
							</div>
							<div class="product-content">
								<div class="all-parts">
									<a class="main-cat" href="<?php echo esc_url( get_term_link( $term->term_taxonomy_id ) ); ?>" ><?php echo esc_html( $term->name ); ?></a>
									<a class="view-parts" href="<?php echo esc_url( get_term_link( $term->term_taxonomy_id ) ); ?>">View All Parts</a>
								</div>
								<p><?php echo esc_html( $term->description ); ?></p>
									<div class="sub-info">
										Subcategories :
										<?php
										// sub category testing code.
										$terms = get_term_children( $term->term_id, 'product_cat' );
											foreach ( $terms as $term ) {
											$subcategory = get_term( $term, 'product_cat' );
											?>
											<a href="<?php echo esc_url( get_term_link( $subcategory->term_taxonomy_id ) ); ?>" ><?php echo esc_html( $subcategory->name ); ?></a>
										<?php
											}
										?>
									</div>
							</div>
						</div>
						<?php
				}
				}

				?>
			</div>
		</div>
	</main>
</div>




<?php
get_footer( 'shop' );
