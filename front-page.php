<?php
/**
 * The template name: Frontpage
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DistiSuite
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
			get_template_part( 'template-parts/content', 'manufacturer' );

			endwhile;


		endif;
		?>

		</main><!-- #main -->
	</div><!-- .primary -->

<section class="featured-post">
	<div class="recent-posts">

		<?php
	$lastposts = get_posts( array(
		'posts_per_page' => 3,
	) );

if ( $lastposts ) {
foreach ( $lastposts as $post ) :
		setup_postdata( $post );
?>
		<article class="contents">
			<div class="blog-post">
				<h2 class="article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="entry-meta">
				<?php echo get_the_date( 'j/m/Y' ); ?>
				</div><!-- .entry-meta -->
	
				<?php echo esc_html( footer_excerpt( 10 ) ); ?>
				<a class="moretag" href="<?php the_permalink(); ?>" rel="bookmark">[Read More]</a>
			</div>
		</article>

	<?php
	endforeach;
	wp_reset_postdata();
}
		?>
	</div>
</section>

<?php
 get_footer();
?>
