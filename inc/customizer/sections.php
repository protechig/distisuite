<?php
/**
 * Customizer sections.
 *
 * @package DistiSuite
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function distisuite_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'distisuite_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'distisuite' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'distisuite_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'distisuite' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'distisuite' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'distisuite_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'distisuite' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'distisuite_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'distisuite' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'distisuite_customize_sections' );
