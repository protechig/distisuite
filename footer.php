<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DistiSuite
 */

?>
</div><!-- #content -->

	<footer class="site-footer">
<div class="footer-nav">
<nav id="site-navigation" class="footer-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'footermenu',
					'menu_id'        => 'footer-menu',
					'menu_class'     => 'menu dropdown',
				) );
			?>
		</nav><!-- #site-navigation -->
</div>
		<div class="site-info">
			<?php distisuite_display_copyright_text(); ?>
			<?php distisuite_display_social_network_links(); ?>
		</div><!-- .site-info -->
	</footer><!-- .site-footer container-->
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

<nav class="off-canvas-container" aria-hidden="true">
	<button type="button" class="off-canvas-close" aria-label="<?php esc_html_e( 'Close Menu', 'distisuite' ); ?>">
		<span class="close"></span>
	</button>
	<?php
		// Mobile menu args.
		$mobile_args = array(
			'theme_location'  => 'mobile',
			'container'       => 'div',
			'container_class' => 'off-canvas-content',
			'container_id'    => '',
			'menu_id'         => 'mobile-menu',
			'menu_class'      => 'mobile-menu',
		);

		// Display the mobile menu.
		wp_nav_menu( $mobile_args );
	?>
</nav>
<div class="off-canvas-screen"></div>
</body>
</html>
