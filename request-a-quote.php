<?php
/**
 * The template Name:Request a Quote.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DistiSuite
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
		<div class="form-description inner-grid">
		<div class="header">
			<h2 class="title"><?php the_field( 'form_header', 'option' ); ?></h2>
		</div>
		<div class="form-desc">
			<?php the_field( 'form_description', 'option' ); ?>
		</div>
		</div>
		<section class="qutation-gf">
			<div class="inner-grid">
				<div class="the-form">
					<?php
						$form = get_field( 'quotation_form', 'option' );
						gravity_form( $form, false, true, false, '', true, 1 );
						?>
				</div>
			</div>
		</section>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
