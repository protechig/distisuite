<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package DistiSuite
 */

// Set up fields.
$animation_class = distisuite_get_animation_class();

// Start a <container> with a possible media background.
distisuite_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container  ', // The class of the container.
) );
?>
	<div class="flexible-content <?php echo esc_attr( $animation_class ); ?>">
		<h2 class="title"> <?php the_sub_field('header') ?></h2>
	<?php
	

	// check if the repeater field has rows of data
	if( have_rows('flexible_content') ):

		// loop through the rows of data
		while ( have_rows('flexible_content') ) : the_row();
		?>
	<div class="flex-product-info">
	<?php
		$image      = get_sub_field( 'media_left' );
		$text       = get_sub_field( 'text_primary' );
		$size		= "refference-image"; // (product custom image size)
		?>
		<div class="product-img">
			<?php echo wp_get_attachment_image( $image, $size ); ?>
		</div>

		<div class="product-desc">
			<?php
				echo force_balance_tags( $text ); // WPCS XSS OK.
			?>
		</div>
	</div>
<?php
	endwhile;
endif;

?>
	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->
