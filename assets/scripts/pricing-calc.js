var price = parseFloat(someVars['regularPrice']);
var discountTable = [];

someVars.discounts.forEach(function (el) {
    discountTable.push({ min: parseFloat(el.min), max: parseFloat(el.max), discount: parseFloat(el.discount) });
})

var sortedTable = discountTable.sort(function (a, b) {
    return a.min - b.min;
})

sortedTable.unshift({ min: 1, max: sortedTable[0].min - 1, discount: 0 });
sortedTable.length
sortedTable[sortedTable.length - 1].max = Infinity

// Calc full price
function calcPrice(qty) {
    for (i = 0; i < sortedTable.length; i++) {
        if (qty >= sortedTable[i].min && qty <= sortedTable[i].max) {
            return Math.round((qty * (price - sortedTable[i].discount)) * 100) / 100;
        }
    }
}

console.log(calcPrice(7))

// Add event listener for qty to change in form

// alternative to DOMContentLoaded
document.addEventListener("DOMContentLoaded", function () {
    // Initialize your application or run some code.

    selectElement = document.querySelector('input.qty');
    selectElement.addEventListener('change', qtyChanged);
    plusElement = document.querySelector('input.plus');
    plusElement.addEventListener('click', qtyChanged);
    minusElement = document.querySelector('input.minus');
    minusElement.addEventListener('click', qtyChanged);


})

function qtyChanged() {
    setTimeout(function () {
        console.log(calcPrice(selectElement.value));
        document.querySelector('.product-price').innerText = 'PRICE $ : ' + calcPrice(selectElement.value)
    }, 001)

}

// Update the innerText of the price element to reflect new total

// select pricing element


// USE VANILLA JS - no jQuery